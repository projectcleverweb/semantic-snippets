<?php

// Everything has a beginning, and this little object is where the magic starts
global $_sys;
$_sys = new stdClass;

// Third Party Includes, because going solo is not nearly as fun as a threesome
require_once SYS_PATH.'/vendor/via-composer/autoload.php';
require_once SYS_PATH.'/vendor/via-github/load.php';

// Ok, now get the stuff we actually did ourselves
require_once SYS_PATH.'/lib/object_loader.php';
