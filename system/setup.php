<?php

// Better (aka pretty) error handling
$_sys->error_handling = new \Whoops\Run;
$_sys->error_handling->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$_sys->error_handling->register();

// Look ma, a database!
try {
	$_sys->db = new PDO(
		sprintf('mysql:host=%1$s;port=%2$s;dbname=%3$s', DB_HOST, DB_PORT, DB_NAME),
		DB_USER,
		DB_PASS
	);
} catch (PDOException $pdo_e) {
	// The database's mom says he's grounded and can't play with us
	throw new Exception('Database Connection Failed: '.$pdo_e->getMessage());
	exit; // Because (this request's) life just isn't worth living anymore
}
