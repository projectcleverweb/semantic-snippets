<?php

global $_sys;

// Get all the things
require_once SYS_PATH.'/bootstrap.php';

// Set all the things
require_once SYS_PATH.'/setup.php';

// Do all the things
app::run();